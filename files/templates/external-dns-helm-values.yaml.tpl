fullnameOverride: ${full_name}

aws:
    region: ${aws_region}
    zoneType: ${route53_zone_type}

${domain_filters}

txtOwnerId: ${cluster_name}

policy: ${dns_record_policy}

rbac:
  create: true
  serviceAccountName: ${service_account_name}
  serviceAccountAnnotations:
    ${service_account_iam_role_annotation}
  apiVersion: v1beta1
  pspEnabled: false

securityContext:
  allowPrivilegeEscalation: false
  readOnlyRootFilesystem: true
  capabilities:
    drop: ["ALL"]
podSecurityContext:
  fsGroup: 1001
  runAsUser: 1001
  runAsNonRoot: true

metrics:
  enabled: true

# Todo: enable these again
livenessProbe: {}
readinessProbe: {}
