locals {
  # List all services that use CMK in the second clause of the conjunction.
  # Services should be combined with disjunction to have any service trigger,
  # the creation of the key.
  # When adding services to the list, make sure to that the CMK IAM policy
  # covers the use case.
  create_cmk = var.enable_kms_encyption #&& (var.install_service_broker || false)

  # DynamoDD Table resource does not yet support CMKs, but when it does,
  # the policy bellow should be required to make it work.
  # Policy reference: https://docs.aws.amazon.com/kms/latest/developerguide/services-dynamodb.html#dynamodb-authz
  # Feature request for CMKs: https://github.com/terraform-providers/terraform-provider-aws/issues/8137
  cmk_dynamodb_policies = [
    {
      sid    = "Allow access through Amazon DynamoDB for all principals in the account that are authorized to use Amazon DynamoDB"
      effect = "Allow"
      principals = [{
        type        = "AWS",
        identifiers = ["*"]
      }]
      actions = [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:CreateGrant",
        "kms:DescribeKey",
      ]
      resources = ["*"]
      condition = [
        {
          test     = "StringEquals"
          variable = "kms:CallerAccount"
          values   = [var.aws_account_id]
        },
        {
          test     = "StringEquals"
          variable = "kms:ViaService"
          values   = ["dynamodb.${var.aws_region}.amazonaws.com"]
        },
      ]
    },
    {
      sid    = "Allow DynamoDB Service with service principal name dynamodb.amazonaws.com to describe the key directly"
      effect = "Allow"
      principals = [{
        type        = "Service",
        identifiers = ["dynamodb.amazonaws.com"]
      }]
      actions = [
        "kms:Describe*",
        "kms:Get*",
        "kms:List*",
      ]
      resources = ["*"]
      condition = []
    },
  ]
}

module "module_cmk" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.4"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  key_name        = "eks-services-${var.project}-${var.environment}-${var.aws_region}"
  key_description = "CMK for resources created by the open-source-devex/terraform-modules/kubernetes/eks-services module"

  create_key = local.create_cmk

  key_policy = concat(
    local.cmk_dynamodb_policies,
    []
  )
}
