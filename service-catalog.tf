locals {
  install_service_catalog = var.install_service_catalog || var.install_service_broker_aws

  service_catalog_name = "svc-cat"

  service_catalog_helm_values = templatefile("${path.module}/files/templates/service-catalog-helm-values.yaml.tpl", {
    replicas              = var.service_catalog_replicas
    health_check_enabled  = var.service_catalog_health_check_enabled
    resources             = jsonencode(var.service_catalog_resources)
    async_binding_enabled = var.service_broker_aws_enable_async_binding
  })
}

resource "helm_release" "service_catalog" {
  count = local.install_service_catalog ? 1 : 0

  name       = local.service_catalog_name
  namespace  = local.service_brokers_namespace
  repository = "https://kubernetes-sigs.github.io/service-catalog"
  chart      = "catalog"
  version    = var.service_catalog_helm_chart_version

  values = concat([local.service_catalog_helm_values], var.service_catalog_helm_values)

  depends_on = [
    kubernetes_namespace.open_service_brokers,
    null_resource.wait_for_tiller,
  ]
}
