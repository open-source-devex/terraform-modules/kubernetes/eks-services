[![pipeline status](https://gitlab.com/open-source-devex/terraform-modules/kubernetes/eks-services/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/terraform-modules/kubernetes/eks-services/-/commits/master)

# eks-services

Terraform module to provision additional services on EKS.

## Services and Integrations

Services:
* metrics_server
* service_catalog
* aws_service_broker
* external_dns
* alb_ingress

Integrations:
* ALB Ingress for Istio


## Terraform docs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| helm | >= 1.3 |
| kubernetes | >= 1.13.0 |

## Providers

| Name | Version |
|------|---------|
| aws | n/a |
| helm | >= 1.3 |
| kubernetes | >= 1.13.0 |
| local | n/a |
| null | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| alb\_ingress\_container\_image\_tag | AWS ALB Ingress Controller docker image tag | `string` | `"v1.1.8"` | no |
| alb\_ingress\_for\_istio\_backend\_protocol | Specifies what the backend protocol should be. Empty means HTTP. | `string` | `"HTTP"` | no |
| alb\_ingress\_for\_istio\_domain\_name | The domain name for the AWS Application LoadBalancer for Istio Ingress Gateway | `string` | `""` | no |
| alb\_ingress\_for\_istio\_gateway | Whether to configure AWS Application LoadBalancer for Istio Ingress Gateway | `bool` | `false` | no |
| alb\_ingress\_for\_istio\_logs\_s3\_bucket | The S3 bucket for the AWS Application LoadBalancer for Istio Ingress Gateway access logs | `string` | `""` | no |
| alb\_ingress\_for\_istio\_logs\_s3\_enabled | Whether to enable access log collection for AWS Application LoadBalancer for Istio Ingress Gateway | `bool` | `false` | no |
| alb\_ingress\_for\_istio\_logs\_s3\_object\_prefix | A prefix to be used for the log objects | `string` | `""` | no |
| alb\_ingress\_for\_istio\_security\_groups | Security Groups to be attached to AWS Application LoadBalancer for Istio Ingress Gateway | `list(string)` | `[]` | no |
| alb\_ingress\_for\_istio\_ssl\_policy | The SSL Policy to configure for AWS Application LoadBalancer for Istio Ingress Gateway | `string` | `"ELBSecurityPolicy-FS-1-2-Res-2019-08"` | no |
| alb\_ingress\_for\_istio\_subnets | The subnets where the AWS Application LoadBalancer for Istio Ingress Gateway must be deployed | `list(string)` | `[]` | no |
| alb\_ingress\_for\_istio\_target\_type | The target type for the AWS Application LoadBalancer for Istio Ingress Gateway target group | `string` | `"ip"` | no |
| alb\_ingress\_for\_istio\_tls\_certificate\_arn | The ARN of ACM certificate for the the HTTPS listener of the AWS Application LoadBalancer for Istio Ingress Gateway | `string` | `""` | no |
| alb\_ingress\_for\_istio\_waf\_acl\_arn | Specifies the ARN for the Amazon WAFv2 Web ACL for AWS Application LoadBalancer for Istio Ingress Gateway | `string` | `""` | no |
| alb\_ingress\_for\_istio\_waf\_enabled | Whether to enable Amazon WAFv2 for AWS Application LoadBalancer for Istio Ingress Gateway | `bool` | `false` | no |
| alb\_ingress\_helm\_chart\_version | AWS ALB Ingress Controller helm chart version | `string` | `"1.0.4"` | no |
| alb\_ingress\_helm\_values | Any additional Helm values to specify to alb-ingress Helm release | `list(string)` | `[]` | no |
| alb\_ingress\_resources | Kubernetes resources for AWS ALB Ingress Controller | `map(map(string))` | <pre>{<br>  "limits": {<br>    "cpu": "100m",<br>    "memory": "128Mi"<br>  },<br>  "requests": {<br>    "cpu": "100m",<br>    "memory": "128Mi"<br>  }<br>}</pre> | no |
| aws\_account\_id | The id of the AWS account thay owns the cluster | `string` | n/a | yes |
| aws\_lb\_container\_image\_tag | AWS Load Balancer Controller docker image tag | `string` | `"v2.0.0"` | no |
| aws\_lb\_helm\_chart\_version | AWS Load Balancer Controller helm chart version | `string` | `"1.0.4"` | no |
| aws\_lb\_helm\_values | Any additional Helm values to specify to aws load balancer Helm release | `list(string)` | `[]` | no |
| aws\_lb\_resources | Kubernetes resources for AWS Load Balancer Controller | `map(map(string))` | <pre>{<br>  "limits": {<br>    "cpu": "100m",<br>    "memory": "128Mi"<br>  },<br>  "requests": {<br>    "cpu": "100m",<br>    "memory": "128Mi"<br>  }<br>}</pre> | no |
| aws\_region | The AWS region where the cluster lives | `string` | n/a | yes |
| cluster\_name | The name of the EKS cluster | `string` | n/a | yes |
| cluster\_tiller\_namespace | The namespace where cluster tiller is installer | `string` | `"kube-system"` | no |
| cluster\_tiller\_wait\_for\_timeout | How much to wait for tiller to be ready before breaking the terraform run | `string` | `"300s"` | no |
| config\_output\_enabled | Whether the module should create optional files like k8s manifests on the file system (under `config_output_path`). Default is true for backwards compatibility. | `bool` | `true` | no |
| config\_output\_path | Where to save files generated by this module | `string` | `"./"` | no |
| create\_services\_namespace | Whether to create a dedicated namespace for the module resources | `bool` | `false` | no |
| enable\_kms\_encyption | Whether to enable encryption at rest with a CMK for AWS resources created by services in this module | `bool` | `false` | no |
| environment | The name of the environment | `string` | n/a | yes |
| external\_dependencies | Outputs from any resource that this module should depend on | `map(string)` | `{}` | no |
| external\_dns\_domain\_filters | List of domain names used to filter hosted zones where external dns shuold create records | `list(string)` | `[]` | no |
| external\_dns\_helm\_chart\_version | The version of the helm chart to use | `string` | `"2.20.4"` | no |
| external\_dns\_helm\_values | Any additional Helm values to specify to external-dns Helm release | `list(string)` | `[]` | no |
| external\_dns\_integration\_isio | Whether to configure interation with istio | `bool` | `false` | no |
| external\_dns\_record\_policy | The policy the controller will use to decide what to do with records (eg. upsert-only does not allow it to remove records) | `string` | `"upsert-only"` | no |
| install\_alb\_ingress | Whether to install ALB Ingress Controller | `bool` | `false` | no |
| install\_aws\_lb | Whether to install AWS Load balancer Controller | `bool` | `false` | no |
| install\_external\_dns | Whether to install External DNS Controller | `bool` | `false` | no |
| install\_metrics\_server | Whether to install the Metrics Server usage data aggregator | `bool` | `false` | no |
| install\_service\_broker\_aws | Whether to install the AWS Service Broker | `bool` | `false` | no |
| install\_service\_catalog | Whether to install Service Catalog | `bool` | `false` | no |
| istio\_namespace | The namespace where Istio is installed | `string` | `"istio-system"` | no |
| kubeconfig | The content for the kubeconfig file to be used when kubectl needs to be used | `string` | `null` | no |
| metrics\_server\_helm\_chart\_version | The version of the helm chart to use | `string` | `"2.11.1"` | no |
| metrics\_server\_helm\_values | Any additional Helm values to specify to external-dns Helm release | `list(string)` | `[]` | no |
| oidc\_provider\_url | The URL (without scheme) of the OpenID Connect provider of the cluster, required when `pod_iam_policy_mode = "iam-oidc"` | `string` | n/a | yes |
| project | The name of the project | `string` | n/a | yes |
| service\_broker\_aws\_container\_image\_with\_tag | n/a | `string` | `"awsservicebroker/aws-servicebroker:1.0.2"` | no |
| service\_broker\_aws\_custom\_templates\_bucket\_name | Name of AWS S3 bucket holding custom templates for AWS Service Broker, leave empty to have the bucket created by the module | `string` | `""` | no |
| service\_broker\_aws\_custom\_templates\_bucket\_prefix | Objects key prefix inside S3 bucket holding custom templates for AWS Service Broker | `string` | `"templates"` | no |
| service\_broker\_aws\_custom\_templates\_bucket\_region | Region of existing S3 bucket holding custom templates for AWS Service Broker | `string` | `""` | no |
| service\_broker\_aws\_custom\_templates\_enabled | Whether configure AWS Service Broker to use custom templates | `bool` | `false` | no |
| service\_broker\_aws\_custom\_templates\_path | Path to a directory containing templates, leave empty to use the templates bundled with the module files | `string` | `""` | no |
| service\_broker\_aws\_enable\_async\_binding | Whether to enable async binding (experimental feature) | `bool` | `false` | no |
| service\_broker\_aws\_helm\_chart\_version | The version of the AWS SB helm chart to use | `string` | `"1.0.2"` | no |
| service\_broker\_aws\_helm\_values | Any additional Helm values to specify to aws-servicebroker Helm release | `list(string)` | `[]` | no |
| service\_broker\_aws\_helm\_wait | Whether helm deploy should wait for all resources to be ready | `bool` | `false` | no |
| service\_broker\_helm\_repo\_url | The url of the helm repo where the chart should be fetched from | `string` | `"https://awsservicebroker.s3.amazonaws.com/charts"` | no |
| service\_catalog\_health\_check\_enabled | n/a | `bool` | `false` | no |
| service\_catalog\_helm\_chart\_version | n/a | `string` | `"0.3.1"` | no |
| service\_catalog\_helm\_values | Any additional Helm values to specify to external-dns Helm release | `list(string)` | `[]` | no |
| service\_catalog\_replicas | How many replicas of the k8s service catalog controllerManager pod count | `number` | `1` | no |
| service\_catalog\_resources | Resources for Service Catalog | `map(map(string))` | <pre>{<br>  "limits": {<br>    "cpu": "200m",<br>    "memory": "400Mi"<br>  },<br>  "requests": {<br>    "cpu": "100m",<br>    "memory": "80Mi"<br>  }<br>}</pre> | no |
| services\_namespace | The name of the namespace where module resources are created | `string` | `"default"` | no |
| tags | Tags to be added to all reosurces that take it | `map(string)` | `{}` | no |
| vpc\_id | The id of the cluster's VPC | `string` | `""` | no |
| wait\_for\_cluster\_tiller | Whether to create a resource that triggers at every run, and uses kubectl to check the readiness state of the cluster tiller pod | `bool` | `false` | no |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
