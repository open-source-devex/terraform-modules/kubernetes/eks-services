locals {
  install_alb_ingress_for_istio = (var.install_alb_ingress || var.install_aws_lb) && var.alb_ingress_for_istio_gateway

  alb_ingress_for_istio_name               = "alb-ingress-for-istio-${var.cluster_name}"
  alb_ingress_for_istio_manifest_file_name = "${local.alb_ingress_for_istio_name}.yaml"
  alb_ingress_for_istio_manifest_file      = join("", local_file.alb_ingress_for_istio.*.filename)

  alb_ingress_for_istio_manifest = local.install_alb_ingress_for_istio ? templatefile("${path.module}/files/templates/alb-ingress-for-istio.yaml.tpl", {
    name                = local.alb_ingress_for_istio_name
    namespace           = var.istio_namespace
    host_name           = var.alb_ingress_for_istio_domain_name
    security_group_ids  = join(",", var.alb_ingress_for_istio_security_groups)
    subnet_ids          = join(",", var.alb_ingress_for_istio_subnets)
    ssl_policy          = var.alb_ingress_for_istio_ssl_policy
    tls_certificate_arn = var.alb_ingress_for_istio_tls_certificate_arn
    tags                = join(",", formatlist("%s=%s", keys(var.tags), values(var.tags)))
    target_type         = var.alb_ingress_for_istio_target_type
    logs_s3_enabled     = var.alb_ingress_for_istio_logs_s3_enabled
    logs_s3_bucket      = var.alb_ingress_for_istio_logs_s3_bucket
    logs_s3_prefix      = var.alb_ingress_for_istio_logs_s3_object_prefix
    waf_enabled         = var.alb_ingress_for_istio_waf_enabled
    waf_acl_arn         = var.alb_ingress_for_istio_waf_acl_arn
    backend_protocol    = var.alb_ingress_for_istio_backend_protocol
  }) : ""
}

# Apply the manifest to the cluster
#   - tries delete on destroy but does not produce any errors
module "alb_ingress_for_istio" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply?ref=v1.0.11"

  enabled = local.install_alb_ingress_for_istio

  enable_delete_on_destroy = true # run kubectl delete on destroy (to prevent leaving an ALB behind)
  silence_delete_errors    = true # don't break terraform destroy if k8s in not responding well to delete

  silence_apply_errors = false # break terraform apply is apply fails

  manifest   = local.alb_ingress_for_istio_manifest
  kubeconfig = local.kubeconfig_file

  detect_drift = true
}

resource "local_file" "alb_ingress_for_istio" {
  count = var.config_output_enabled && local.install_alb_ingress_for_istio ? 1 : 0

  filename = "${var.config_output_path}/${local.alb_ingress_for_istio_manifest_file_name}"
  content  = local.alb_ingress_for_istio_manifest
}
