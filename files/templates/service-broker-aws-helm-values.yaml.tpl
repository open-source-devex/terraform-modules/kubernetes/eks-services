image: ${container_image_with_tag}

aws:
  region: ${aws_region}
  tablename: ${dynamodb_table_name}

brokerconfig:
  brokerId: ${broker_id}
