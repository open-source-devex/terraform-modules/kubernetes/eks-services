fullnameOverride: ${name_override}
clusterName: ${cluster_name}
region: ${region}
vpcId: ${vpc_id}

rbac:
  create: true

serviceAccount:
  create: true
  name: ${service_account_name}
  annotations: ${service_account_iam_role_annotation}

image:
  tag: ${container_image_tag}
  pullPolicy: IfNotPresent

resources: ${resources}
