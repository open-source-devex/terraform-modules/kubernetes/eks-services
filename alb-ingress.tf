locals {
  alb_ingress_name      = "alb-ingress"
  alb_ingress_namespace = local.services_namespace

  alb_ingress_helm_values = templatefile("${path.module}/files/templates/alb-ingress-helm-values.yaml.tpl", {
    name_override = local.alb_ingress_name
    cluster_name  = var.cluster_name

    aws_region               = var.aws_region
    auto_discover_aws_region = false
    vpc_id                   = var.vpc_id
    auto_discover_vpc        = false

    container_image_tag = var.alb_ingress_container_image_tag

    service_account_name = local.alb_ingress_name

    service_account_iam_role_annotation = jsonencode({
      (local.eks_iam_service_account_annotation) = module.alb_ingress_iam_role.this_iam_role_arn
    })

    resources = jsonencode(var.alb_ingress_resources)
  })

  alb_ingress_iam_resource_name = "k8s-${local.alb_ingress_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

resource "helm_release" "alb_ingress" {
  count = var.install_alb_ingress ? 1 : 0

  name       = local.alb_ingress_name
  namespace  = local.alb_ingress_namespace
  repository = "https://charts.helm.sh/incubator"
  chart      = "aws-alb-ingress-controller"
  version    = var.alb_ingress_helm_chart_version

  values = concat([local.alb_ingress_helm_values], var.alb_ingress_helm_values)

  depends_on = [
    null_resource.wait_for_tiller,
  ]
}

# Create IAM role with required policies for AWS ALB Ingress Controller
module "alb_ingress_iam_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 2.22"

  create_role = var.install_alb_ingress

  role_name = local.alb_ingress_iam_resource_name

  provider_url     = var.oidc_provider_url
  role_policy_arns = [join("", aws_iam_policy.alb_ingress.*.arn)]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.alb_ingress_namespace}:${local.alb_ingress_name}"]

  tags = var.tags
}

resource "aws_iam_policy" "alb_ingress" {
  count = var.install_alb_ingress ? 1 : 0

  name   = local.alb_ingress_iam_resource_name
  policy = join("", data.aws_iam_policy_document.alb_ingress_iam_role_policy.*.json)

}

# Source: https://github.com/kubernetes-sigs/aws-alb-ingress-controller/blob/master/docs/examples/iam-policy.json
data "aws_iam_policy_document" "alb_ingress_iam_role_policy" {
  count = var.install_alb_ingress ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "acm:DescribeCertificate",
      "acm:ListCertificates",
      "acm:GetCertificate",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup",
      "ec2:CreateTags",
      "ec2:DeleteTags",
      "ec2:DeleteSecurityGroup",
      "ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses",
      "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus",
      "ec2:DescribeInternetGateways",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeTags",
      "ec2:DescribeVpcs",
      "ec2:ModifyInstanceAttribute",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:RevokeSecurityGroupIngress",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "elasticloadbalancing:AddListenerCertificates",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:CreateListener",
      "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateRule",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener",
      "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteRule",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:DescribeListenerCertificates",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:DescribeSSLPolicies",
      "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:RemoveTags",
      "elasticloadbalancing:SetIpAddressType",
      "elasticloadbalancing:SetSecurityGroups",
      "elasticloadbalancing:SetSubnets",
      "elasticloadbalancing:SetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "iam:CreateServiceLinkedRole",
      "iam:GetServerCertificate",
      "iam:ListServerCertificates",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "cognito-idp:DescribeUserPoolClient",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf-regional:GetWebACLForResource",
      "waf-regional:GetWebACL",
      "waf-regional:AssociateWebACL",
      "waf-regional:DisassociateWebACL"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "tag:GetResources",
      "tag:TagResources",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf:GetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "wafv2:GetWebACL",
      "wafv2:GetWebACLForResource",
      "wafv2:AssociateWebACL",
      "wafv2:DisassociateWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "shield:DescribeProtection",
      "shield:GetSubscriptionState",
      "shield:DeleteProtection",
      "shield:CreateProtection",
      "shield:DescribeSubscription",
      "shield:ListProtections"
    ]

    resources = ["*"]
  }
}

resource "local_file" "alb_ingress_helm_release_values" {
  count = var.config_output_enabled && var.install_alb_ingress ? 1 : 0

  filename = "${var.config_output_path}/${local.alb_ingress_name}-helm-values.yaml"
  content  = local.alb_ingress_helm_values
}
